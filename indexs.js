var TelegramBot = require('node-telegram-bot-api');
var TOKEN = '624915283:AAHik5mUzvS7w-97uKH3Viq7dIDGKC0G2v4';
var mysql = require('mysql');
var sql;
var connection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'botduk'
});
console.log('bot has been startet');
connection.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});
var bot = new TelegramBot(TOKEN, {
    webHook:{
        port: 3000
    }
});
bot.setWebHook(`https://2658997d.ngrok.io/bot${TOKEN}`);
var admin1 ;
var admin2 ;
var userid;
var userid1;
var admin_call = {
    reply_markup: JSON.stringify({
        keyboard: [
            ['Написать админу'],
        ]
    })
};
var admin_answer = {
    reply_markup: JSON.stringify({
        keyboard: [
            [`Answer`],
        ]
    })
};
var close = {
    reply_markup: JSON.stringify({
        keyboard: [
            [`закончить`],
        ]
    })
};
var us1=0;
var us2=0;
var ad2= 0;
var ad1 = 0;
var br1 = 0;
var br2 = 0;
var firstanswer;
var firstanswer2;
var menu ={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: '🛏️ Номера ', callback_data: 'room' },{ text: '🍽️ Ресторан ', callback_data: 'restoran' } ,{ text: '☀️ Спа Центр', callback_data: 'spa' }],
            [{ text: '🎁 Акции ', callback_data: 'gift' },{ text: '💬 Вопрос-ответ ', callback_data: 'quest' } ,{ text: '🔍 Как добраться ', callback_data: 'find' }],
        ]
    })
};
var room ={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'Президентский ', callback_data: 'pre_room' },{ text: 'Люкс с балконом ', callback_data: 'l_b_room' } ,{ text: 'Люкс', callback_data: 'luks_room' } ,{ text: 'Полулюкс', callback_data: 'p_l_room' }],
            [{ text: 'Стандарт улучшенный ', callback_data: 'stan_l_room' },{ text: 'Стандарт ', callback_data: 'stand_room' } ,{ text: 'Мансард ', callback_data: 'mansa_room' }],
            [{ text: 'Назад', callback_data: 'back' }],
        ]
    })
};
var restoran = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'Завтраки ', callback_data: 'breacfast' },{ text: 'Бизнес-ланчи ', callback_data: 'lunch' }],
            [{ text: 'Рум-сервис ', callback_data: 'room_service' },{ text: 'Основное меню', callback_data: 'osn_menu' }],
        ]
    })
};
var spa={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'Время работы ', callback_data: 'time_rabota' },{ text: 'Тренажеры Кардио зал ', callback_data: 'trena' }],
        ]
    })
};
var quest = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'Заезд / Выезд', callback_data: 'zaezd' },{ text: 'Что включено в проживание', callback_data: 'in_pro' } ],
            [{ text: 'Как изменить / отменить свою \n существующую бронь?', callback_data: 'bron' },{ text: 'Отзывы', callback_data: 'otzuv' }],
            [{ text: 'Проживание с животными', callback_data: 'liveg' },{ text: 'Парковка', callback_data: 'parkovka' }],
            [{ text: 'Назад', callback_data: 'back' }],
        ]
    })
};
/*ROOM*/
var pre_room={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'ФОТО ', callback_data: 'pre_photo' },{ text: 'Посмотреть на сайте ', url:'https://www.hotel-duke.com/hotel-rooms/presidential-suite' },{ text: 'Забронировать', callback_data: 'buy_pre' }],
            [{ text: 'Посмотреть другой номер ', callback_data: 'back_room' }],
        ]
    })
};
var l_b_room={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'ФОТО ', callback_data: 'l_b_photo' },{ text: 'Посмотреть на сайте ', url:'https://www.hotel-duke.com/hotel-rooms/suite-with-balcony' },{ text: 'Забронировать', callback_data: 'buy_l_b' }],
            [{ text: 'Посмотреть другой номер ', callback_data: 'back_room' }],
        ]
    })
};
var l_room={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'ФОТО ', callback_data: 'l_photo' },{ text: 'Посмотреть на сайте ', url:'https://www.hotel-duke.com/hotel-rooms/suite' },{ text: 'Забронировать', callback_data: 'buy_l' }],
            [{ text: 'Посмотреть другой номер ', callback_data: 'back_room' }],
        ]
    })
};
var p_l_room={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'ФОТО ', callback_data: 'p_l_photo' },{ text: 'Посмотреть на сайте ', url:'https://www.hotel-duke.com/hotel-rooms/junior-suite' },{ text: 'Забронировать', callback_data: 'buy_p_l' }],
            [{ text: 'Посмотреть другой номер ', callback_data: 'back_room' }],
        ]
    })
};
var stan_l_room={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'ФОТО ', callback_data: 'stan_l_photo' },{ text: 'Посмотреть на сайте ', url:'https://www.hotel-duke.com/hotel-rooms/standard-superior' },{ text: 'Забронировать', callback_data: 'buy_stan_l' }],
            [{ text: 'Посмотреть другой номер ', callback_data: 'back_room' }],
        ]
    })
};
var stand_room={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'ФОТО ', callback_data: 'stand_photo' },{ text: 'Посмотреть на сайте ', url:'https://www.hotel-duke.com/hotel-rooms/standard' },{ text: 'Забронировать', callback_data: 'buy_stand' }],
            [{ text: 'Посмотреть другой номер ', callback_data: 'back_room' }],
        ]
    })
};
var mars_room={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'ФОТО ', callback_data: 'mars_photo' },{ text: 'Посмотреть на сайте ', url:'https://www.hotel-duke.com/hotel-rooms/standard-mansard' },{ text: 'Забронировать', callback_data: 'buy_mars' }],
            [{ text: 'Посмотреть другой номер ', callback_data: 'back_room' }],
        ]
    })
};
/*RESTORAN*/
var breakfast = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'Основное меню ', callback_data: 'osn_menu' },{ text: 'Вопрос-ответ', callback_data: 'quest' }],
            [{ text: 'Назад', callback_data: 'back' }],
        ]
    })
};
var osnmenu ={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: '💬 Вопрос-ответ ', callback_data: 'quest' },{ text: 'Назад', callback_data: 'back' }],
        ]
    })
};
/*ВОПРОСЫ ОТВЕТЫ*/
var zaezd = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: 'Способ оплаты', callback_data: 'oplata' }],
            [{ text: 'Ранний заезд / поздний выезд', callback_data: 'p_zaezd' }],
            [{ text: 'Назад', callback_data: 'back' }],
        ]
    })
};
var whatp={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: '💬 Вопрос-ответ ', callback_data: 'quest' }],
        ]
    })
};
var map ={
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: '🗺️ Посмотреть на карте', callback_data: 'map' }],
        ]
    })
}
bot.onText(/\/start/, function (msg, match){
    bot.sendMessage(msg.chat.id,`Здравствуйте,${msg.from.first_name} . `,admin_call);
    bot.sendMessage(msg.chat.id,`Я бот  отеля Дюк и я помогу Вам в брониронии номера в нашем отеле в самом сердце Одессы, расскажу о дополнительных услугах, а также отвечу на имеющиеся вас вопросы.\n
Выберите интересующий Вас пункт меню ниже:`,menu)

    var user_id={
        user_login:msg.from.username,
        user:msg.from.first_name,
        msg_id_bot:msg.chat.id,
    };
    connection.query('INSERT INTO user  SET ?',user_id);
});
bot.onText(/\/admin/, function (msg, match) {
    admin1 = msg.chat.id;
    bot.sendMessage(msg.chat.id , `now you admin`);
});
bot.onText(/\/pic/, function (msg, match) {
    admin2 = msg.chat.id;
    bot.sendMessage(msg.chat.id , `now you admin2`);
});
bot.on('callback_query',(callbackQuery) => {
    const msg = callbackQuery.message;
    switch (callbackQuery.data) {
        /*Нлавное меню*/
        case "room":
            bot.sendMessage(msg.chat.id, 'Номера', room);
            break
        case "gift":

            break
        case "restoran":
            bot.sendMessage(msg.chat.id, 'Ресторан ', restoran);
            break
        case "spa":
            bot.sendMessage(msg.chat.id, 'Восстановить силы и жизненный тонус после насыщенного событиями дня, вы можете в СПА отеля Дюк.  Здесь гостей ожидает бассейн площадью 35 кв.м. и зона отдыха.\n' +
                'Для любителей банных процедур имеется финская сауна и хамам.\n' +
                'Дополнить ощущения отдыха и внутренней гармонии помогут восстановительные Спа-массажи с аромамаслами.  ', spa);
            break
        case "find":
            bot.sendMessage(msg.chat.id, 'Отель Дюк находится по адресу: Украина, г. Одесса, пер. Чайковского, 10\n' +
                'Телефон: +38 (048) 705 -36 -36', map);
            break
        case "quest":
            bot.sendMessage(msg.chat.id, 'Вопрос-ответ', quest);
            break
        /*Комнаты*/
        case "pre_room":
            bot.sendMessage(msg.chat.id,`Роскошный Президентский номер состоит из просторной гостиной с зоной отдыха, 
уютной спальни с двуспальной кроватью, мраморной ванны и гостевой туалетной комнаты. 
Широкой платяной шкаф, комод и компактные тумбы помогут удобно разместить вещи гостя. 
В рабочей зоне имеется письменный стол с принадлежностями.`, pre_room);
            break
        case "l_b_room":
            bot.sendMessage(msg.chat.id,`Номер Люкс с балконом состоит из двух комнат: спальни, с кроватью и письменным столом в рабочей зоне, и гостиной с диваном и уютными креслами в зоне отдыха.
 Просторная мраморная ванная комната обустроена для полного комфорта гостя: душевая кабина и ванна.
С балкона номера открывается вид на переулок Чайковского и Театр Оперы и Балета`, l_b_room);
            break
        case "lucks_room":
            bot.sendMessage(msg.chat.id,`Просторные двухкомнатные номера категории Люкс состоят из гостиной комнаты, спальни, ванной и гостевой туалетной комнаты.
Ванная комната, отделанная мрамором, предусматривает удобства для обоих гостей, включая двойную раковину, отдельный душ и ванну.`, l_room);
            break
        case "p_l_room":
            bot.sendMessage(msg.chat.id,`Интерьер номера Полулюкс, разработанный по авторскому дизайну, учитывает все, что может понадобиться, чтобы окунуться в атмосферу комфорта.
Рабочий стол позволит продуктивно поработать, а мягкая двуспальная кровать и обустроенная зона отдыха возле окна создадут уют и удобства, чтобы расслабиться и восстановить силы.`, p_l_room);
            break
        case "stan_l_room":
            bot.sendMessage(msg.chat.id,`Классический интерьер номера категории Улучшенный стандарт выполнен в светлых и мягких тонах. В номере расположены и удачно совмещены, рабочая зона и зона отдыха с двуспальной кроватью, журнальным столиком и креслами.
Возможно размещение дополнительной кровати.`, stan_l_room);
            break
        case "stand_room":
            bot.sendMessage(msg.chat.id,`Однокомнатный двухместный номер категории Стандарт идеально подходит как для туристов, так и бизнес путешественников. Уютные светлые номера с итальянской мебелью выполнены по авторскому дизайну. Планировка номера, удачно дополненная современными элементами, лаконично совмещает рабочую зону с зоной отдыха.`, stand_room);
            break
        case "mans_room":
            bot.sendMessage(msg.chat.id,`Уютные и светлые однокомнатные номера категории Стандарт Мансардный находятся на верхнем этаже отеля. Просторная планировка комнаты позволяет комфортно расположиться двум гостям.
Все номера данной категории “Twin” (с двумя раздельными кроватями).`, mars_room);
            break




        case "back_room":
            bot.sendMessage(msg.chat.id, 'Номера', room);
            break
        case "back":
            bot.sendMessage(msg.chat.id, 'Главное меню', menu);
            break





        /*презеденская комната*/
        case "pre_photo":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',pre_room);
            break
        case "buy_pre":
            if(userid == null && userid1 != msg.chat.id) {
                br1 = 1;
                userid = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br1
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin1, `Пользователь ${msg.from.first_name}
Хочет заказать Президентский номер`,admin_answer);
                bot.sendMessage(msg.chat.id, 'Please hold, the administrator will contact you.');
            }
            if(userid1 == null && userid != msg.chat.id){
                br2 = 1;
                userid1 = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br2
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin2, `Пользователь ${msg.from.first_name}
Хочет заказать Президентский номер`,admin_answer)
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 != msg.chat.id && userid != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin1, `Пользователь:${msg.from.first_name}
Пытался с вами связаться
Хотел забронировать Президентский номер
id:${msg.chat.id}
nickname:${msg.from.username}
        `)
            }
            break

        /*люкс с балконом комната*/
        case "l_b_photo":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',pre_room);
            break
        case "buy_l_b":
            if(userid == null && userid1 != msg.chat.id) {
                br1 = 1;
                userid = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br1
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin1, `Пользователь ${msg.from.first_name}
Хочет заказать Люкс с балконом`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 == null && userid != msg.chat.id){
                br2 = 1;
                userid1 = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br2
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin2, `Пользователь ${msg.from.first_name}
Хочет заказать Люкс с балконом`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 != msg.chat.id && userid != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
Пытался с вами связаться
Хотел забронировать Президентский номер
id:${msg.chat.id}
nickname:${msg.from.username}
        `)
            }
            break

        /*Люкс комната*/
        case "l_photo":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',pre_room);
            break
        case "buy_l":
            if(userid == null && userid1 != msg.chat.id) {
                br1 = 1;
                userid = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br1
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin1, `Пользователь ${msg.from.first_name}
Хочет заказать Люкс`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 == null && userid != msg.chat.id){
                br2 = 1;
                userid1 = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br2
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin2, `Пользователь ${msg.from.first_name}
Хочет заказать Люкс`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 != msg.chat.id && userid != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
Пытался с вами связаться
Хотел забронировать Президентский номер
id:${msg.chat.id}
nickname:${msg.from.username}
        `)
            }
            break

        /*Полу люкс комната*/
        case "p_l_photo":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',pre_room);
            break
        case "buy_p_l":
            if(userid == null && userid1 != msg.chat.id) {
                br1 = 1;
                userid = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br1
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin1, `Пользователь ${msg.from.first_name}
Хочет заказать Полу люкс`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 == null && userid != msg.chat.id){
                br2 = 1;
                userid1 = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br2
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin2, `Пользователь ${msg.from.first_name}
Хочет заказать Полу люкс`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 != msg.chat.id && userid != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
Пытался с вами связаться
Хотел забронировать Президентский номер
id:${msg.chat.id}
nickname:${msg.from.username}
        `)
            }
            break

        /*Стандарт улучшеная комната*/
        case "stan_l_photo":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',pre_room);
            break
        case "buy_stan_l":
            if(userid == null && userid1 != msg.chat.id) {
                br1 = 1;
                userid = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br1
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin1, `Пользователь ${msg.from.first_name}
Хочет заказать Стандартную улучшеную `);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 == null && userid != msg.chat.id){
                br2 = 1;
                userid1 = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br2
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin2, `Пользователь ${msg.from.first_name}
Хочет заказать Стандартную улучшеную `);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 != msg.chat.id && userid != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
Пытался с вами связаться
Хотел забронировать Президентский номер
id:${msg.chat.id}
nickname:${msg.from.username}
        `)
            }
            break

        /*стандартная комната*/
        case "stand_photo":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',pre_room);
            break
        case "buy_stand":
            if(userid == null && userid1 != msg.chat.id) {
                br1 = 1;
                userid = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br1
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin1, `Пользователь ${msg.from.first_name}
Хочет заказать Стандарт`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 == null && userid != msg.chat.id){
                br2 = 1;
                userid1 = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br2
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin2, `Пользователь ${msg.from.first_name}
Хочет заказать Стандарт`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 != msg.chat.id && userid != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
Пытался с вами связаться
Хотел забронировать Президентский номер
id:${msg.chat.id}
nickname:${msg.from.username}
        `)
            }
            break
        /*Мансардный комната*/
        case "mars_photo":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',pre_room);
            break
        case "buy_mars":
            if(userid == null && userid1 != msg.chat.id) {
                br1 = 1;
                userid = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br1
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin1, `Пользователь ${msg.from.first_name}
Хочет заказать Стандарт Мансардный`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 == null && userid != msg.chat.id){
                br2 = 1;
                userid1 = msg.chat.id;
                var user_m={
                    user:msg.from.first_name,
                    user_message:msg.text,
                    msg_chat_id:msg.chat.id,
                    bron:br2
                };
                connection.query('INSERT INTO message  SET ?',user_m);
                bot.sendMessage(admin2, `Пользователь ${msg.from.first_name}
Хочет заказать Стандарт Мансардный`);
                bot.sendMessage(msg.chat.id, 'Ожидайте, сейчас с Вами свяжется администратор');
            }
            if(userid1 != msg.chat.id && userid != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
Пытался с вами связаться
Хотел забронировать Президентский номер
id:${msg.chat.id}
nickname:${msg.from.username}
        `)
            }
            break


        /*Ресторан*/
        /*Завтраки*/
        case "breacfast":
            bot.sendMessage(msg.chat.id, 'Завтрак в формате Шведского стола подается c 07:00 до 11:00 в ресторане ЛенМар, расположенном на I-м этаже отеля.\n' +
                'Завтрак включен в проживание гостей отеля.\n' +
                '(+ фото ресторана, гости завтракают)', breakfast);
            break
        /* Бизнес ланч*/
        case "lunch":

            break
        /*Основное меню*/
        case "osn_menu":
            bot.sendPhoto(msg.chat.id,'./pre_room.jpg',osnmenu)
            break
        /*Рум-сервис*/
        case "room_service":
            bot.sendMessage(msg.chat.id, 'Обслуживание в номерах в отеле Дюк работает круглосуточно. В каждом номере имеется папка \n' +
                'с предлагаемым меню Рум сервиса.\n' +
                'Заказать еду или напитки в номер вы можете, позвонив по внутреннему номеру.', breakfast);
            break

        /*Спа*/
        case "time_rabota":
            bot.sendMessage(msg.chat.id, 'Наш СПА-центр работает 24 часа.\n' +
                'Посетить сауну и хаммам вы можете с 07:00 до 23:00.\n' +
                'Посещение Спа-центра входит в проживание гостей', questt);
            break
        case "trena":
            bot.sendMessage(msg.chat.id, 'Для гостей, которые придерживаются спортивного образа жизни, СПА-центр оборудован кардиотренажерами фирмы Matrix (велотренажер, беговая дорожка и пр.). Здесь гости могут провести тренировку для поддержания формы и тонуса мышц. ', questt);
            break

        /*Вопросы и ответы*/
        case "zaezd":
            bot.sendMessage(msg.chat.id, 'Время заезда 14:00.\n' +
                'Время выезда 12:00.', zaezd);
            break
        case "in_pro":
            bot.sendMessage(msg.chat.id, 'В проживание в отеле Дюк включено: \n' +
                '- завтрак\n' +
                '- посещение СПА центра \n' +
                '(бассейн, хаммам, сауна)\n' +
                '- посещение тренажерного зала\n' +
                '- скидка -10% в ресторане ЛенМар', whatp);
            break
        case "bron":
            bot.sendMessage(msg.chat.id, ' Изменить или аннулировать бронь вы можете, связавшись с нами по телефонам \n' +
                '+38 (048) 705 -37 -73 и +38 (048) 705 -36 -36', whatp);
            break
        case "otzuv":

            break
        case "liveg":
            bot.sendMessage(msg.chat.id, 'Мы знаем как вы любите своих питомцев, но в отеле Дюк не предусмотрены условия для проживания животных.\n' +
                'Благодарим за понимание.',whatp );
            break
        case "parkovka":
            bot.sendMessage(msg.chat.id, 'Отель Дюк предоставляет гостям возможность припарковать автобиль рядом с отелем. В наличии 10 парковочных мест.', whatp);
            break
        case "p_zaezd":
            bot.sendMessage(msg.chat.id, 'Если Вы хотите воспользоваться услугой раннего заезда или поздннего выезда, пожалуйста, уведомьте менеджера отеля. Оплата данной услуги: ранний заезд - 50 % стоимости номера; при выезде в течение 12 часов после расчетного часа оплата –  50% стоимости номера, а при выезде свыше 12 часов после расчетного часа –  100% стоимости номера. Данная услуга предоставляется при наличии свободных номеров.', whatp);
            break
        case "oplata":
            bot.sendMessage(msg.chat.id, 'Оплатить проживание и прочие услуги отеля Дюк, вы можете воспользовавшись наличным  или безналичным способом оплаты', whatp);
            break
        case "map":
            bot.sendLocation(msg.chat.id , 46.485421,30.742317)
            break
    }
})
bot.on('message', msg=>{
    if(userid == null || msg.chat.id!=userid || userid1 == null || msg.chat.id!=userid1){
        if(msg.text=="Написать админу"&& userid == null  && msg.chat.id != admin1){

            userid = msg.chat.id;
            bot.sendMessage(userid,"Напишити свой вопрос");
            bot.sendMessage(admin1,`Пользовать пишет сообщение ${msg.from.first_name}`);

        }
        if(msg.text=="Написать админу"&&  userid1 == null && msg.chat.id != admin1&& msg.chat.id!= userid){
            userid1 = msg.chat.id;
            bot.sendMessage(admin2,`Пользовать пишет сообщение ${msg.from.first_name}`);
            bot.sendMessage(userid1,"Напишити свой вопрос");
        }
        if(msg.text=="Написать админу"&& userid != msg.chat.id && userid1 != msg.chat.id  && msg.chat.id != admin1){
            bot.sendMessage(msg.chat.id , "Извините все заняты");
            bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
        Пытался с вами связаться
        id:${msg.chat.id}
        nickname:${msg.from.username}
        `)
        }

        if( msg.text !="/start" && msg.text != "/admin" && msg.text != "/pic"  && msg.chat.id == admin1 && userid == null ){
            bot.sendMessage(admin1, "Пользователей нету ");
        }
        if( msg.text !="/start" && msg.text != "/admin" && msg.text != "/pic" && msg.chat.id == admin2 && userid1 == null){
            bot.sendMessage(admin2, "Пользователей нету ");
        }
        if(msg.text !="/start" && msg.text != "/admin" && msg.text != "/pic"  && msg.chat.id != admin1 && msg.chat.id != admin2 && msg.text!="Написать админу" && userid != msg.chat.id && userid1 != msg.chat.id ){
            bot.sendMessage(msg.chat.id, "Пожалуйста нажминте кнопку для связи с админом");
        }
    }
    if(userid == msg.chat.id || msg.chat.id== admin1){
        if( msg.text!="Написать админу"&& us1==0  && msg.chat.id != admin1){
            firstanswer =msg.text ;
            bot.sendMessage(msg.chat.id,"Отправляю админу");
            us1 = 1;

        }
        if( msg.text!="Написать админу"&& us1==1  && msg.chat.id != admin1){
            var user_m={
                user:msg.from.first_name,
                user_message:msg.text,
                msg_chat_id:msg.chat.id,
                bron:br1
            };
            connection.query('INSERT INTO message  SET ?',user_m);
            bot.sendMessage(admin1,msg.text,close);

        }
        setTimeout(function () {
            if (ad1 != 1 && userid1 ==null){
                userid1 = userid;
                ad2= 0;
                bot.sendMessage(admin2,firstanswer,admin_answer);
                userid = null;
            }
            if(ad1 != 1 && userid1 != null && userid1 != msg.chat.id){
                bot.sendMessage(msg.chat.id , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
        Пытался с вами связаться
        id:${msg.chat.id}
        nickname:${msg.from.username}
        `)
            }
        },10000);
        if( msg.text=="Answer" && ad1==0 && msg.chat.id == admin1){
            ad1=1;
            bot.sendMessage(msg.chat.id,"Теперь вы можете ответить пользователю");
            bot.sendMessage(userid,"Админ отвечает");

        }
        if( msg.text!="Answer" && ad1==1 && msg.chat.id == admin1){
            var admin_m={
                admin:msg.from.first_name,
                answer:msg.text,
                msg_user_id:userid,
            };
            connection.query('INSERT INTO admin_answer  SET ?',admin_m);
            bot.sendMessage(userid,msg.text);
        }
        if( msg.text!="Answer" && ad1==0 && msg.chat.id == admin1){
            bot.sendMessage(admin1,"Пожалуйста перед тем как писать ответ , нажмите на кнопку (Ответить)",admin_answer);
        }
        if( msg.text=="закончить"&& ad1==1 && msg.chat.id == admin1){
            bot.sendMessage(admin1,`Диалог с пользователем закончен`);
            bot.sendMessage(userid,`Диалог с админом прекращен`);
            ad1=0;
            userid = null;
        }
    }
    if(firstanswer != null){
        setTimeout(function () {
            if (ad2 == 0 && msg.chatid != admin2 && msg.chatid != admin1){
                bot.sendMessage(userid1 , "Извините все заняты");
                bot.sendMessage(admin2, `Пользователь:${msg.from.first_name}
        Пытался с вами связаться
        id:${msg.chat.id}
        nickname:${msg.from.username}
        `, {reply_markup: JSON.stringify({
                        inline_keyboard: [
                            [{ text: `${userid1}`, callback_data: `${userid1}` }],
                        ]
                    })});
                userid1 = null;
            }
        },10000);
    }
    if(userid1 == msg.chat.id || msg.chat.id== admin2){

        if( msg.text!="Написать админу"&& us2==0  && msg.chat.id != admin2){
            us2 = 1;
            firstanswer2 =msg.text ;
            bot.sendMessage(msg.chat.id,"Отправляю админу");

        }
        if( msg.text!="Написать админу"&& us2==1  && msg.chat.id != admin2){
            var user_m={
                user:msg.from.first_name,
                user_message:msg.text,
                msg_chat_id:msg.chat.id,
                bron:br2
            };
            connection.query('INSERT INTO message  SET ?',user_m);
            bot.sendMessage(admin2,msg.text,close);

        }
        if( msg.text=="Answer"&& ad2==0 && msg.chat.id == admin2){
            ad2=1;
            var admin_m={
                admin:msg.from.first_name,
                answer:msg.text,
                msg_user_id:userid1,
            };
            connection.query('INSERT INTO admin_answer  SET ?',admin_m);
            bot.sendMessage(userid1,msg.text);

        }
        if( msg.text!="Answer"&& ad2==1 && msg.chat.id == admin2){
            var admin_m={
                admin:msg.from.first_name,
                answer:msg.text,
                msg_user_id:userid1,
            };
            connection.query('INSERT INTO admin_answer  SET ?',admin_m);
            bot.sendMessage(userid1,msg.text);
        }
        if( msg.text!="Answer"&& ad2==0 && msg.chat.id == admin2){

            bot.sendMessage(admin2,"Пожалуйста перед тем как писать ответ , нажмите на кнопку (Ответить)");
        }
        if( msg.text=="закончить"&& ad2==1 && msg.chat.id == admin2){
            bot.sendMessage(admin2,`Диалог с пользователем закончен`);
            bot.sendMessage(userid1,`Диалог с админом прекращен`);
            ad2=0;
            userid1 = null;
        }
    }

});
bot.on('callback_query', function (msg) {
    if (msg.data > 0){
        ad2=1;
        userid1= msg.data;
        bot.sendMessage(admin2, "связываюсь с пользователем");
        bot.sendMessage(userid1, "дминистратор на связи . пожалуйста нажмите на кнопку(Написать администратору)", admin_call);
    }
});